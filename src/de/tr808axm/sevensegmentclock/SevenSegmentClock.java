package de.tr808axm.sevensegmentclock;

import java.text.DecimalFormat;
import java.util.Calendar;

/**
 * This is a test, should be made in <2h.
 * Created by tr808axm on 08.07.2016.
 */
public class SevenSegmentClock implements Runnable {
    public static final String[] colonStrings = new String[]{"   ", " o ", " o "};

    public static void main(String[] args) {
        new SevenSegmentClock();
    }

    public SevenSegmentClock() {
        Thread timer = new Thread(this);
        timer.start();
    }

    public static String[] convertTimeToSevenSegmentStrings() {
        DecimalFormat df = new DecimalFormat("00");
        char[] hours = df.format(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)).toCharArray();
        char[] minutes = df.format(Calendar.getInstance().get(Calendar.MINUTE)).toCharArray();
        char[] seconds = df.format(Calendar.getInstance().get(Calendar.SECOND)).toCharArray();
        return SevenSegment.concatSevenSegmentStrings(new String[][]{
                SevenSegment.parseNumber(hours[0]).toStrings(),
                SevenSegment.parseNumber(hours[1]).toStrings(),
                colonStrings,
                SevenSegment.parseNumber(minutes[0]).toStrings(),
                SevenSegment.parseNumber(minutes[1]).toStrings(),
                colonStrings,
                SevenSegment.parseNumber(seconds[0]).toStrings(),
                SevenSegment.parseNumber(seconds[1]).toStrings()
        });
    }

    public static void printSevenSegmentStrings(String[] sevenSegmentStrings) {
        String printableString = "";
        for(String line : sevenSegmentStrings) {
            printableString += "\n" + line;
        }
        System.out.println(printableString);
    }

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                printSevenSegmentStrings(convertTimeToSevenSegmentStrings());
            }
        } catch (InterruptedException e) {
            System.out.println("Timer interrupted.");
        }
    }
}
